package com.nbf.api;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.data.Protocol;
import org.restlet.routing.Router;

import com.nbf.api.resources.DefaultResource;
import com.nbf.api.resources.LoginResource;
import com.nbf.api.resources.SignUpResource;

public class NeighborfoodApplication extends Application {

	@Override
	public Restlet createInboundRoot() {
		Router router = new Router(getContext());
		getConnectorService().getClientProtocols().add(Protocol.HTTP);
		router.attachDefault(DefaultResource.class);
		router.attach("/signup", SignUpResource.class);
		router.attach("/login", LoginResource.class);
		return router;
	}

}

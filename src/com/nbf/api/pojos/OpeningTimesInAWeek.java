package com.nbf.api.pojos;

import java.sql.Time;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Describes which days of the week and which hours of each day the shop is
 * open.
 */
// TODO: create newInstance method if necessary. Constructors may not be
// correct.
public class OpeningTimesInAWeek {
	private ArrayList<OpeningTimesInADay> timetable;

	public OpeningTimesInAWeek(ArrayList<JSONObject> jsonObjects) {
		JSONObject object;
		OpeningTimesInADay openingTimesInDay;
		timetable = new ArrayList<OpeningTimesInADay>();

		for (int i = 0; i < jsonObjects.size(); i++) {
			object = jsonObjects.get(i);
			try {
				openingTimesInDay = new OpeningTimesInADay(
						DayOfTheWeek.fromJasonString(object.getString("day")),
						Time.valueOf(object.getString("start_time")),
						Time.valueOf(object.getString("end_time")));
				timetable.add(openingTimesInDay);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	public OpeningTimesInAWeek(JSONArray jsonObjects) {
		JSONObject object = null;
		timetable = new ArrayList<OpeningTimesInADay>();
		OpeningTimesInADay openingTimesInDay;
		for (int i = 0; i < jsonObjects.length(); i++) {
			try {
				object = jsonObjects.getJSONObject(i);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			if (object != null) {
				try {
					openingTimesInDay = new OpeningTimesInADay(
							DayOfTheWeek.fromJasonString(object
									.getString("day")), Time.valueOf(object
									.getString("start_time")),
							Time.valueOf(object.getString("end_time")));
					timetable.add(openingTimesInDay);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public ArrayList<OpeningTimesInADay> getWeeklyTimetable() {
		return timetable;
	}

	public void setWeeklyTimetable(ArrayList<OpeningTimesInADay> timetable) {
		this.timetable = timetable;
	}

}

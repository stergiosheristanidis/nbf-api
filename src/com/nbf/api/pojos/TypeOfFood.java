package com.nbf.api.pojos;

public enum TypeOfFood {
	UNKNOWN(Json.UNKNOWN), CHINESE(Json.CHINESE), PIZZA(Json.PIZZA);
	
	private String value;
	
	public static TypeOfFood fromJsonString(String value){
		if(value == null || value.equals(null) || value.equals(Json.UNKNOWN)){
			return UNKNOWN;
		}else if(value.equals(Json.CHINESE)){
			return CHINESE;
		}else if(value.equals(Json.PIZZA)){
			return PIZZA;
		}
		return UNKNOWN;
	}
	
	public String toString(){
		return value;
	}
	private TypeOfFood(String value){
		this.value = value;
		
	}

	private static class Json{
		private static final String UNKNOWN = "UNKNOWN";
		private static final String PIZZA = "PIZZA";
		private static final String CHINESE = "CHINESE";

	}
	
}

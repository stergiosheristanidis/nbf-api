package com.nbf.api.pojos;


public enum DayOfTheWeek{

	MONDAY(Json.MONDAY), TUESDAY(Json.TUESDAY), WEDNESDAY(Json.WEDNESDAY), THURSDAY(
			Json.THURSDAY), FRIDAY(Json.FRIDAY), SATURDAY(Json.SATURDAY), SUNDAY(
			Json.SUNDAY);

	private final String value;

	private DayOfTheWeek(String value) {
		this.value = value;
	}
	
	public static DayOfTheWeek fromJasonString(String value){
		if(value == null || value.equals(null)|| value.equals("")){
			throw new IllegalArgumentException();
		}
		if(value.equals(Json.MONDAY)){
			return MONDAY;
		}else if(value.equals(Json.TUESDAY)){
			return TUESDAY;
		}else if(value.equals(Json.WEDNESDAY)){
			return WEDNESDAY;
		}else if(value.equals(Json.THURSDAY)){
			return THURSDAY;
		}else if(value.equals(Json.FRIDAY)){
			return FRIDAY;
		}else if(value.equals(Json.SATURDAY)){
			return SATURDAY;
		}else if(value.equals(Json.SUNDAY)){
			return SUNDAY;
		}
		return null;
		
	}

	private static class Json {
		private static final String MONDAY = "MONDAY";
		private static final String TUESDAY = "TUESDAY";
		private static final String WEDNESDAY = "WEDNESDAY";
		private static final String THURSDAY = "THURSDAY";
		private static final String FRIDAY = "FRIDAY";
		private static final String SATURDAY = "SATURDAY";
		private static final String SUNDAY = "SUNDAY";
	}
}

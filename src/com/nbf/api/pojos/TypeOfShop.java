package com.nbf.api.pojos;

public enum TypeOfShop {
	RESTAURANT(Json.RESTAURANT), FASTFOOD(Json.FASTFOOD), UNKNOWN(Json.UNKNOWN);
	
	private String value;
	
	private TypeOfShop(final String value){
		this.value = value;
	}
	
	public String toString(){
		return value;
	}
	public static TypeOfShop fromJsonString(String value){
		if(value == null){
			return null;
		}else if (value.equals(Json.RESTAURANT)){
			return TypeOfShop.RESTAURANT;
		}else if(value.equals(Json.FASTFOOD)){
			return TypeOfShop.FASTFOOD;
		}else{
			return TypeOfShop.UNKNOWN;
		}
	}
	
	private static class Json{
		private static final String RESTAURANT = "RESTAURANT";
		private static final String FASTFOOD = "FASTFOOD";
		private static final String UNKNOWN = "UNKNOWN";

	}
	
	

}

package com.nbf.api.pojos;

import java.util.ArrayList;

/**
 * This represents an owner of one or more shops.
 */
public class Owner {

	private String email; // TODO: Should be unique. Need validation.
	private String password;
	private ArrayList<Shop> shops;

	/**
	 * Creates a new {@link Owner} object, requiring the email and password.
	 * 
	 * @param email
	 * @param password
	 */
	public Owner(String email, String password) {
		this.email = email;
		this.password = password;
	}

	public Owner(String email, String password, Shop shop) {
		this.email = email;
		this.password = password;
		shops = new ArrayList<Shop>();
		shops.add(shop);
	}

	public Owner(String email, String password, ArrayList<Shop> shops) {
		this.email = email;
		this.password = password;
		this.shops = shops;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public ArrayList<Shop> getShops() {
		return shops;
	}

	public void setShops(ArrayList<Shop> shops) {
		this.shops = shops;
	}

}

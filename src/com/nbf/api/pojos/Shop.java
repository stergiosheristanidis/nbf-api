package com.nbf.api.pojos;

public class Shop {

	private TypeOfShop typeOfShop;
	private String brandName;
	private TypeOfFood typeOfFood;
	private OpeningTimesInAWeek openingTimesInAWeek;

	public Shop(TypeOfShop typeOfShop, String brandName, TypeOfFood typeOfFood,
			OpeningTimesInAWeek openingTimes) {
		this.typeOfShop = typeOfShop;
		this.brandName = brandName;
		this.typeOfFood = typeOfFood;
		this.openingTimesInAWeek = openingTimes;
	}

	public OpeningTimesInAWeek getOpeningTimes() {
		return openingTimesInAWeek;
	}

	public void setOpeningTimes(OpeningTimesInAWeek openingTimes) {
		this.openingTimesInAWeek = openingTimes;
	}

	public String getTypeOfFood() {
		return typeOfFood.toString();
	}

	public void setTypeOfFood(TypeOfFood typeOfFood) {
		this.typeOfFood = typeOfFood;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getTypeOfShop() {
		return typeOfShop.toString();
	}

	public void setTypeOfShop(TypeOfShop typeOfShop) {
		this.typeOfShop = typeOfShop;
	}

}

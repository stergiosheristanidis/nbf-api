package com.nbf.api.pojos;

import java.sql.Time;
/**
 * This class describes the opening times in a specific day.
 */
public class OpeningTimesInADay {
	private DayOfTheWeek day;
	private Time startTime;
	private Time endTime;

	public OpeningTimesInADay(DayOfTheWeek day, Time startTime, Time endTime) {
		this.day = day;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public DayOfTheWeek getDay() {
		return day;
	}

	public void setDay(DayOfTheWeek day) {
		this.day = day;
	}

	public Time getStartTime() {
		return startTime;
	}

	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}

	public Time getEndTime() {
		return endTime;
	}

	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}

}

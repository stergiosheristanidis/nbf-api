package com.nbf.api.resources;

import org.json.JSONObject;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Entity;

public class LoginResource extends ServerResource {
	
	@Post("json")
	public String loginUser(String info){
		JSONObject json;
		System.out.println("Post works for login!!");
		try{
			json = new JSONObject(info);
			String username = json.getString("username");
			String password = json.getString("password");
			System.out.println("ok me json");
			long checkCode = checkUser(username, password);
			System.out.println("doylevei" + "" + checkCode);
			json = new JSONObject();
			//boolean flag;
			if(checkCode<0){
				json.put("success", false);
				//flag = false;
				System.out.println("false");
				
			}else{
				json.put("success", true);
				//flag=true;
				System.out.println("true");
				json.put("user_id", checkCode);
			}
			System.out.println(json.toString());			
			return json.toString();
			
			
		}catch(Exception e){
			return null;
		}
	}

	/**
	 * Check if the info provided is a valid login
	 * @param name Username
	 * @param pass Password
	 * @param uID UserID
	 * @return True if the user with the provided name has the provided password and uID. False if not.
	 */
	@SuppressWarnings("deprecation")
	@Post
	private long checkUser(String username, String password) {
	
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		Query q = new Query("owner");
		q.addFilter("username", FilterOperator.EQUAL, username);
		PreparedQuery pq = ds.prepare(q);
		
		//User doesn't exist
		if(pq.countEntities() == 0) return -1;
		
		//User exist
		Entity e = pq.asSingleEntity();
		
		if(!((String) e.getProperty("username")).equals(username)) return -1;
		if(!((String) e.getProperty("password")).equals(password)) return -2;
		
		System.out.println("tha eprpepe na emfanizei checkcode" + e.getKey());
		
		return e.getKey().getId();
		
	}
	
}


	


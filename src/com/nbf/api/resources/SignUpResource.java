package com.nbf.api.resources;

import org.json.JSONObject;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.nbf.api.pojos.OpeningTimesInAWeek;
import com.nbf.api.pojos.Owner;
import com.nbf.api.pojos.Shop;
import com.nbf.api.pojos.TypeOfFood;
import com.nbf.api.pojos.TypeOfShop;

public class SignUpResource extends ServerResource {

	@Post("json")
	public String signUpOwner(String info) {
		JSONObject json;
		try {
			json = new JSONObject(info);
			String username = json.getString("username");
			String password = json.getString("password");
			String brandName = json.getString("brand_name");
			TypeOfShop typeOfShop = TypeOfShop.fromJsonString(json
					.getString("type_of_shop"));
			TypeOfFood typeOfFood = TypeOfFood.fromJsonString(json
					.getString("type_of_food"));
			OpeningTimesInAWeek openingTimes = new OpeningTimesInAWeek(
					json.getJSONArray("opening_times"));
			Shop shop = new Shop(typeOfShop, brandName, typeOfFood,
					openingTimes);
			Owner owner = new Owner(username, password, shop);
			long id = signUpOwner(owner, shop);
			json = new JSONObject();
			if (id < 0) {
				json.put("success", false);
			} else {
				json.put("success", true);
			}
			json.put("owner_id", id);
			return json.toString();
		} catch (Exception e) {
			return null;
		}

	}

	@Post
	public long signUpOwner(Owner owner, Shop shop) {
		long id = -1;
		DatastoreService datastoreService = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query("Owner");
		q.addFilter("username", FilterOperator.EQUAL, owner.getEmail());

		PreparedQuery pq = datastoreService.prepare(q);
		Query q2 = new Query("openingTimes");
		PreparedQuery pq2 = datastoreService.prepare(q2);

		//Set filter to check if email exists.
		FetchOptions fetchOptions = FetchOptions.Builder.withLimit(10);
		
		System.out.println("pq.countEntities = " + pq.countEntities(fetchOptions));
		if(pq.countEntities(fetchOptions) == 0){
			Entity newOwner = new Entity("Owner");
			newOwner.setProperty("username", owner.getEmail());
			newOwner.setProperty("password", owner.getPassword());
			datastoreService.put(newOwner);
			Key ownerKey = newOwner.getKey();
			
			Entity newShop = new Entity("Shop", ownerKey);
			newShop.setProperty("brandName", shop.getBrandName());
			newShop.setProperty("typeOfShop", shop.getTypeOfShop());
			newShop.setProperty("typeOfFood", shop.getTypeOfFood());
			datastoreService.put(newShop);
			Key shopKey = newShop.getKey();

			Entity openingTimes = new Entity("OpeningTimes", shopKey);
			for (int i = 0; i < shop.getOpeningTimes().getWeeklyTimetable().size(); i++) {
				System.out.println("signUpOwner"
						+ shop.getOpeningTimes().getWeeklyTimetable().get(i).getDay());
				openingTimes.setProperty("day", shop.getOpeningTimes()
						.getWeeklyTimetable().get(i).getDay().toString());
				openingTimes.setProperty("startTime", shop.getOpeningTimes()
						.getWeeklyTimetable().get(i).getStartTime().toString());
				openingTimes.setProperty("endTime", shop.getOpeningTimes()
						.getWeeklyTimetable().get(i).getEndTime().toString());
			}
			datastoreService.put(openingTimes);
			id = newOwner.getKey().getId();
		}
		
		return id;
	}
}
